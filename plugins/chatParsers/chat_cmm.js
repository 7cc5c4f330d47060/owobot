const parsePlain = require('../../util/chatparse_plain.js')
module.exports = {
  parse: (data, b) => {
    if (data.type === 'system' || data.type === 'legacy') {
      if (data.json.translate === '%s %s › %s' || data.json.translate === '[%s] %s › %s') {
        let subtype = 'chipmunkmod_'
        if (data.json.translate === '%s %s › %s') {
          subtype += 'name3'
        } else if (data.json.translate === '[%s] %s › %s') {
          subtype += 'chomens'
        }
        if (data.json.with && data.json.with[1] && data.json.with[2]) {
          const username = parsePlain(data.json.with[1])
          const uuid = b.findUUID(username)
          const nickname = b.findDisplayName(uuid)
          const message = parsePlain(data.json.with[2].extra)
          return {
            parsed: true,
            json: data.json,
            type: data.type,
            subtype,
            uuid,
            message,
            nickname,
            username
          }
        } else {
          subtype += '_invalid'
          return {
            parsed: true,
            json: data.json,
            type: data.type,
            subtype,
            uuid: '00000000-0000-0000-0000-000000000000',
            message: '',
            nickname: '',
            username: ''
          }
        }
      }
    }
    return {
      parsed: false
    }
  },
  priority: 0
}
